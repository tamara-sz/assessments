N = 5

def draw_pascal_triangle():
    spaces = " " * N
    line,c,p = "", "", 1
    for i in range(N):
        spaces = spaces[0:N-i]
        c = ""
        for j in range(p):
            c += "1"
        line += spaces + c + "\n"
        p += 2
    return line

print(draw_pascal_triangle())

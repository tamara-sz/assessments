def is_leap(year):
    leap = False
    if year % 100 == 0 and year % 400 == 0:
        leap = True
    elif year % 4 == 0 and not year % 100 == 0:
        leap = True
    if year <= 1900 or year >= 2100:  # in this period 13 days are the difference between Julian and Gregorian calendar
        leap = False                  # in other period this difference is more or less days, so it can be not correct with this function.
    return leap

is_leap(int(input("Give a year, please! ")))

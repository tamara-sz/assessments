import unittest
from leap_year import is_leap


class LeapYearsTest(unittest.TestCase):
    def test_with_1895(self):
        self.assertFalse(is_leap(1895))
    
    def test_with_2000(self):
        self.assertTrue(is_leap(2000))

    def test_with_2104(self):
        self.assertFalse(is_leap(2104))

    def test_with_1995(self):
        self.assertFalse(is_leap(1995))


if __name__ == '__main__':
    unittest.main()
